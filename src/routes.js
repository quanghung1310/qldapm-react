import React from 'react';
import { Navigate } from 'react-router-dom';
import DashboardLayout from './layouts/DashboardLayout';
import MainLayout from './layouts/MainLayout';
import AccountView from './views/account';
import TrashView from './views/trash';
import DashboardView from './views/dashboard/Dashboard';
import LoginView from './views/auth/LoginView';
import NotFoundView from './views/errors/NotFoundView';
import RegisterView from './views/auth/RegisterView';
import SettingsView from './views/settings/SettingsView';

const routes = (loggedIn) => [
  {
    path: 'app',
    element: loggedIn ? <DashboardLayout /> : <Navigate to="/login" />,
    children: [
      { path: 'account', element: <AccountView /> },
      { path: 'trash', element: <TrashView /> },
      { path: 'dashboard', element: <DashboardView /> },
      { path: 'settings', element: <SettingsView /> },
      { path: '*', element: <NotFoundView /> }
    ]
  },
  {
    path: '/',
    element: !loggedIn ? <MainLayout /> : <Navigate to="/app/dashboard" />,
    children: [
      { path: 'login', element: <LoginView /> },
      { path: 'register', element: <RegisterView /> },
      { path: '404', element: <NotFoundView /> },
      { path: '/', element: <Navigate to="/login" /> },
      { path: '*', element: <Navigate to="/404" /> }
    ]
  }
];

export default routes;
